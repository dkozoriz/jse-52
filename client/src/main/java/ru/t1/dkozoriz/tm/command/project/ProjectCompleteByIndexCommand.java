package ru.t1.dkozoriz.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.project.ProjectCompleteByIndexRequest;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    public ProjectCompleteByIndexCommand() {
        super("project-complete-by-index", "complete project by index.");
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        getEndpointLocator().getProjectEndpoint()
                .projectCompleteByIndex(new ProjectCompleteByIndexRequest(getToken(), index));
    }

}